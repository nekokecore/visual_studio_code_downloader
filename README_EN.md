# Official Visual Studio Code Downloader

> This is a tool to download the official Visual Studio Code for mainland China network environment. Based on the system architecture, it retrieves the corresponding download link and downloads the latest version of the Visual Studio Code installer.

[English](README_EN.md) / [中文](README.md)

## Table of Contents

- [Usage](#usage)
- [Notes](#notes)
- [Disclaimer](#disclaimer)
- [License](#license)
- [Acknowledgements](#acknowledgements)

## Usage

1. Ensure that Rust programming language runtime environment is installed on your computer.
2. Download and compile the source code:

```bash
git https://gitee.com/nekokecore/visual_studio_code_downloader
cd visual_studio_code_downloader
cargo build --release --target i686-pc-windows-msvc
```

3. Run the generated executable file:

```bash
./visual_studio_code_downloader.exe
```

The tool will automatically detect the system architecture and download the latest version of Visual Studio Code. Once the download is complete, it will be saved with the `.exe` extension. Note that this tool only supports the Windows operating system.

## Notes

- If your system architecture is not `x86` or `x86_64`, the tool will not be supported and display an error message indicating the unsupported system.
- Ensure a stable network connection for successful download of the Visual Studio Code installer.
- By default, the tool uses 32 concurrent connections with a block size of 4 MB for downloads. If you need to modify these settings, you can edit the relevant parameters in the source code.

## Disclaimer

This tool is intended to assist users in downloading the official version of Visual Studio Code. However, no guarantees are made regarding the security, integrity, or legality of its contents. Users assume all risks and responsibilities associated with the use of this tool.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Acknowledgements

We would like to express our sincere gratitude to [ycysdf](https://github.com/ycysdf) for their contribution in implementing the functionality with their [code](https://github.com/ycysdf/http-downloader-tui).
