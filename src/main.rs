use anyhow::Result;
use clap::{arg, Parser};
use crossterm::cursor::{Hide, MoveToColumn, MoveToNextLine, MoveToPreviousLine};
use crossterm::execute;
use crossterm::style::Print;
use crossterm::terminal::{Clear, ClearType};
use http_downloader::bson_file_archiver::{ArchiveFilePath, BsonFileArchiverBuilder};
use http_downloader::{
    breakpoint_resume::DownloadBreakpointResumeExtension,
    speed_limiter::DownloadSpeedLimiterExtension, speed_tracker::DownloadSpeedTrackerExtension,
    HttpDownloaderBuilder,
};
use regex::Regex;
use reqwest::redirect::Policy;
use reqwest::Client;
use std::fmt::Write;
use std::fs::rename;
use std::io::stdout;
use std::num::{NonZeroU8, NonZeroUsize};
use std::path::PathBuf;
use std::process::{Command, Stdio};
use std::str::FromStr;
use std::time::{Duration, Instant};
use tokio::time::sleep;
use url::Url;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    url: Url,

    #[arg(short, long, default_value_t = NonZeroU8::new(3).unwrap())]
    connection_count: NonZeroU8,

    #[arg(long, default_value_t = NonZeroUsize::new(1024 * 1024 * 4).unwrap())]
    chunk_size: NonZeroUsize,

    #[arg(short, long, default_value = None)]
    speed_limit: Option<usize>,

    #[arg(short, long, default_value_t = true)]
    progress: bool,

    #[arg(long, default_value_t = false)]
    silence: bool,
}

#[tokio::main]
async fn main() {
    println!("=== 正版visual studio code下载工具 ===");
    loop {
        let env_os_bit = std::env::consts::ARCH;
        let url_str = if is_win7() {
            get_the_actual_url(
                "https://vscode.cdn.azure.cn/stable/a21a160d630530476218b85db95b0fd2a8cd1230/VSCodeUserSetup-ia32-1.70.3.exe",
            )
            .await
        } else {
            match env_os_bit {
                "x86" => {
                    get_the_actual_url(
                        "https://code.visualstudio.com/sha/download?build=insider&os=win32-user",
                    )
                    .await
                }
                "x86_64" => get_the_actual_url(
                    "https://code.visualstudio.com/sha/download?build=insider&os=win32-x64-user",
                )
                .await,
                _ => {
                    println!("不支持当前系统：{}", env_os_bit);
                    "不支持当前系统".to_string()
                }
            }
        };
        let mut download_url = Url::parse(&url_str).unwrap();
        // 由于Visual Studio Code官方团队换了新的更新网址，
        // 现在https://vscode.cdn.azure.cn不能用了，
        // 新的网址是https://vscode.download.prss.microsoft.com
        download_url.set_host(Some("vscode.download.prss.microsoft.com")).unwrap();
        let actual_url = download_url.to_string();

        let args = Args {
            url: Url::from_str(&actual_url).unwrap(),
            connection_count: NonZeroU8::new(32).unwrap(),
            chunk_size: NonZeroUsize::new(1024 * 1024 * 4).unwrap(),
            speed_limit: None,
            progress: true,
            silence: false,
        };
        let file_path_result = do_download(args).await;
        if file_path_result.is_ok() {
            let file_path = file_path_result.unwrap();
            sleep(Duration::from_secs(3)).await;
            rename(file_path.clone(), format!("{}.exe", file_path)).unwrap();
            break;
        }
    }
}

async fn do_download(args: Args) -> Result<String> {
    let result = std::env::current_exe()?;
    let save_dir = result.parent().unwrap().to_path_buf();
    // println!(
    //     "current url: {}, save_dir: {}, is dir: {}",
    //     args.url,
    //     save_dir.display(),
    //     save_dir.is_dir()
    // );
    let http_downloader_builder = HttpDownloaderBuilder::new(args.url, save_dir);
    let (downloader, (speed_state, ..)) = http_downloader_builder
        .download_connection_count(args.connection_count)
        .chunk_size(args.chunk_size)
        .build((
            DownloadSpeedTrackerExtension { log: false },
            DownloadSpeedLimiterExtension {
                byte_count_per: args.speed_limit,
            },
            DownloadBreakpointResumeExtension {
                download_archiver_builder: BsonFileArchiverBuilder::new(ArchiveFilePath::Suffix(
                    "bson".to_string(),
                )),
            },
        ));
    let file_path = downloader.get_file_path();
    let file_path_copy = file_path.clone();
    execute!(stdout(), Hide)?;
    let mut is_first = true;
    let finished_future = downloader.start().await?;
    if !args.silence && args.progress {
        let mut bar = ProgressBar::new(62);
        tokio::spawn({
            let mut downloaded_len_receiver = downloader.downloaded_len_receiver().clone();
            async move {
                let total_len = downloader.total_size().await;
                while downloaded_len_receiver.changed().await.is_ok() {
                    let downloaded_len = *downloaded_len_receiver.borrow();
                    if let Some(total_len) = total_len {
                        let buf = bar.update(
                            downloaded_len,
                            total_len.into(),
                            speed_state.download_speed(),
                        )?;
                        if is_first {
                            execute!(stdout(), Print(buf))?;
                            is_first = false;
                        } else {
                            execute!(
                                stdout(),
                                Clear(ClearType::CurrentLine),
                                MoveToPreviousLine(1),
                                Clear(ClearType::CurrentLine),
                                MoveToColumn(0),
                                Print(buf)
                            )?;
                        }
                    }

                    tokio::time::sleep(Duration::from_millis(100)).await;
                }
                Result::<PathBuf>::Ok(file_path)
            }
        });
    }

    let downloading_end_cause = finished_future.await?;

    if !args.silence {
        execute!(
            stdout(),
            MoveToNextLine(1),
            Clear(ClearType::CurrentLine),
            MoveToPreviousLine(1),
            Clear(ClearType::CurrentLine),
            MoveToColumn(0),
            Print(format!("{:?}", downloading_end_cause)),
        )?;
    }
    Result::<String>::Ok(file_path_copy.display().to_string())
}

pub struct ProgressBar {
    bar_buf: String,
    buf: String,
    start_instant: Instant,
    bar_width: usize,
}

impl ProgressBar {
    pub fn new(max_width: usize) -> Self {
        Self {
            buf: String::new(),
            bar_buf: String::new(),
            start_instant: Instant::now(),
            bar_width: crossterm::terminal::size()
                .ok()
                .map(|(cols, _rows)| usize::from(cols))
                .unwrap_or(0)
                .min(max_width),
        }
    }
    fn update(
        &mut self,
        downloaded_len: u64,
        total_len: u64,
        speed: u64,
    ) -> Result<&str, std::fmt::Error> {
        let progress = (downloaded_len * 100 / total_len) as usize;

        let (downloaded_len_size, downloaded_len_unit) = Self::byte_unit(downloaded_len);
        let (total_len_size, total_len_unit) = Self::byte_unit(total_len);
        let (speed_size, speed_unit) = Self::byte_unit(speed);

        self.bar_buf.clear();
        self.buf.clear();
        let duration = self.start_instant.elapsed();
        write!(
            self.bar_buf,
            "{speed_size:.2} {speed_unit}/s - {progress} % - elapsed: {duration:.2?} "
        )?;
        write!(
            self.buf,
            "{downloaded_len_size:.2} {downloaded_len_unit} / {total_len_size:.2} {total_len_unit}"
        )?;
        for _ in 0..(self.bar_width - self.bar_buf.len() - self.buf.len()) {
            self.bar_buf.push(' ');
        }
        writeln!(self.bar_buf, "{}", self.buf)?;

        let bar_p_width = self.bar_width - 2;
        let progress_width = progress * bar_p_width / 100;
        self.bar_buf.push('[');
        for _ in 0..progress_width {
            self.bar_buf.push('█');
        }
        for _ in progress_width..bar_p_width {
            self.bar_buf.push(' ');
        }
        self.bar_buf.push(']');

        Ok(&self.bar_buf)
    }

    fn byte_unit(bytes_count: u64) -> (f32, &'static str) {
        const UNITS: [&str; 6] = ["B", "KB", "MB", "GB", "TB", "PB"];

        let mut i = 0;
        let mut bytes_count = bytes_count as f32;
        while bytes_count >= 1024.0 && i < UNITS.len() - 1 {
            i += 1;
            bytes_count /= 1024.0;
        }
        (bytes_count, UNITS[i])
    }
}

async fn get_the_actual_url(url: &str) -> String {
    let client = Client::builder()
        .redirect(Policy::custom(|attempt| {
            if attempt.previous().len() > 5 {
                attempt.error("Too many redirects")
            } else {
                attempt.follow()
            }
        }))
        .build()
        .unwrap();

    let resp = client.get(url).send().await.unwrap();

    return resp.url().to_string();
}

fn is_win7() -> bool {
    let output = Command::new("wmic")
        .args(["os", "get", "version"])
        .current_dir("./")
        .stdout(Stdio::piped())
        .spawn();

    match output {
        Ok(child) => {
            let output = child.wait_with_output().expect("failed to execute process");

            if output.status.success() {
                let stdout = String::from_utf8_lossy(&output.stdout).to_string();
                let re = Regex::new(r"\d+\.\d+").unwrap();

                if let Some(version) = re.find(&stdout) {
                    if version.as_str().eq("6.1") {
                        println!("{}", "是win7系统");
                        return true;
                    }
                } else {
                    println!("未找到版本号");
                }
            } else {
                let stderr = String::from_utf8_lossy(&output.stderr);
                eprintln!("版本获取失败：\n{}", stderr);
                std::process::exit(-1);
            }
            return false;
        }
        Err(_) => return false,
    }
}
