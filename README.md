# 正版 Visual Studio Code 下载工具

> 这是一个用于适用于大陆网络环境的下载正版 Visual Studio Code 的工具。根据系统的位数，它将获取相应的下载链接并下载最新版本的 Visual Studio Code 安装程序。

[English](README_EN.md) / [中文](README.md)

## 目录

- [使用方法](#使用方法)
- [注意事项](#注意事项)
- [免责声明](#免责声明)
- [许可证](#许可证)
- [致谢](#致谢)

## 使用方法

1. 确保你的计算机已经安装 Rust 编程语言的运行环境。
2. 下载并编译源代码：

```bash
git clone https://gitee.com/nekokecore/visual_studio_code_downloader
cd visual_studio_code_downloader
cargo build --release --target i686-pc-windows-msvc
```

3. 运行生成的可执行文件：

```bash
./visual_studio_code_downloader.exe
```

工具将自动检查系统的位数，并下载最新版本的 Visual Studio Code。下载完成后，将会以`.exe`的扩展名保存。请注意，该工具仅支持 Windows 操作系统。

## 注意事项

- 如果你的系统位数不是`x86`或`x86_64`，工具将无法支持。运行时，它将显示一条不支持当前系统的错误信息。
- 请确保你的网络连接稳定，以便工具能够成功下载 Visual Studio Code 的安装程序。
- 工具将默认使用 32 个并发连接，下载的块大小为 4 MB。如果需要修改这些设置，你可以编辑源代码中相关的参数。

## 免责声明

这个工具旨在帮助用户下载正版的 Visual Studio Code，但不对其内容的安全性、完整性或合法性做出任何保证。用户需要自行承担使用该工具所产生的风险和责任。

## 许可证

本项目基于 MIT License 进行许可 - 有关详细信息，请参阅 [LICENSE](LICENSE) 文件。

## 致谢

我们在实现功能中使用了 [ycysdf](https://github.com/ycysdf) 的[代码](https://github.com/ycysdf/http-downloader-tui)，对他的贡献表示感谢，并向他表达最诚挚的谢意。
